import csv, re
import app_constants

n_postcodes = 0;
n_passes = 0;
fail_log = [];
chunkSize = 100000
file_output = "regex-check-failures.csv"

postcodeRegex = []
# Supplied
# postcodeRegex.append() 
# postcodeRegex.append(re.compile('([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})'))
# # Standard UK regex
# postcodeRegex.append(re.compile('^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$'))
# # UK regex including overseas addresses
# postcodeRegex.append(re.compile('^((GIR &0AA)|((([A-PR-UWYZ][A-HK-Y]?[0-9][0-9]?)|(([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRV-Y]))) &[0-9][ABD-HJLNP-UW-Z]{2}))$'))

def main():
    with open(app_constants.input_file) as file:
        reader = csv.DictReader(file)
        for row in reader:
            # print(row)
            n_postcodes = n_postcodes+1
            reg = re.compile('[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z][A-Z]')
            if(n_postcodes % chunkSize == 0):
                print(str(chunkSize)+" chunk evaluated, Tally of failures: " + str(len(fail_log)))

            if (reg.match(row["Postcode"])):
                pass
            else:
                # print ("FAIL")
                failure_item = {}
                failure_item["postcode"] = row["Postcode"]
                # failure_item["inUse"] = row["In Use?"]
                fail_log.append(failure_item)

        print("Postcodes Evaluated: "+str(n_postcodes))
        print("Failures Found: "+str(len(fail_log)))

        with open(file_output, 'w') as f:
            for item in fail_log:
                f.write("%s\n" % item["postcode"] ) # + ", " + item["inUse"])
        print("Output written to file" + file_output)

if __name__ == '__main__':
    main()
