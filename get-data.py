import urllib2, json
import app_constants

def add_user_devices(self, serial):
    # (url, access_token, api_token) = self.get_api_conf()
    api_url = self.url + "/api/v1/user/devices"
    token = self.access_token + " " + self.api_token

    data = {'serial': serial}
    request = urllib2.Request(api_url, json.dumps(data))
    request.add_header('Authorization', token)
    request.add_header('Content-Type', 'application/json')
    try:
        urllib2.urlopen(request)
    except Exception, e:
        print e.code
        print e.read()


def writeFile(fileContents):
    with open(app_constants.input_file, 'w') as f:
        f.write(fileContents)
    print("Output written to file" + app_constants.input_file)


def main():
    data_base_uri = 'https://www.doogal.co.uk/'
    data_path = ['UKPostcodesCSV.ashx?Search=BF', 'files/postcodes.zip']

    print(data_base_uri + data_path[0])

    request = urllib2.Request(data_base_uri + data_path[0])
    try:
        writeFile(urllib2.urlopen(request).read())
    except Exception, e:
        print e.code
        print e.read()

if __name__ == '__main__':
    main()
