import csv, time
import app_constants

def main():
    t0=time.time()
    with open(app_constants.input_file) as f:
        reader = csv.DictReader(f)
        in_use_count = total_count = not_in_use_count = invalid_count = 0
        for row in reader:
            total_count = total_count + 1
            if row["In Use?"] == "Yes":
                in_use_count = in_use_count + 1
            elif row["In Use?"] == "No":
                not_in_use_count = not_in_use_count + 1
            else:
                invalid_count = invalid_count + 1

        print("Total Rows: "+str(total_count))
        print("Total In Use: "+str(in_use_count))
        print("Total Not Use: "+str(not_in_use_count))
        print("Total Invalid: "+str(invalid_count))
        print('Elapsed time : ', time.time() - t0)


if __name__ == '__main__':
    main()
