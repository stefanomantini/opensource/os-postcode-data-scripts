import csv, time
import app_constants

nRows = 2

def main():
    print('Getting first ' + str(nRows) + ' rows')
    t0 = time.time()
    with open(app_constants.input_file, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        idx = 0
        for row in csv_reader:
            if idx < nRows:
                print(', '.join(row))
            else:
                print('Elapsed time : ', time.time() - t0)
                break
            idx = idx + 1

if __name__ == '__main__':
    main()
