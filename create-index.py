import pymongo
from pymongo import IndexModel, ASCENDING, DESCENDING
import connection

def main():
    connection.db = client['TEST']
    pcodes = db.postcodes
    people = db.people
    logger = db.log

    print("\nPre: Indexes")
    for index in people.index_information():
        print(index)


    index1=IndexModel([("postcode", ASCENDING)], name="_people_postcode_")
    people.create_indexes([index1])

    print("\nPost: Indexes")
    for index in people.index_information():
        print(index)

if __name__ == '__main__':
    main()
