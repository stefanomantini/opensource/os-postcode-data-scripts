import csv
import app_constants

def main():
    with open(app_constants.input_file) as file:
        reader = csv.DictReader(file)
        count = 0
        print("Postcode, County, District, Last Updated")
        for row in reader:
            if row["In Use?"]:
                print(row["Postcode"] + ", " + row["County"] + ", " + row["District"], row["Last updated"])
                count = count + 1
        print("Total In Use: "+str(count))

if __name__ == '__main__':
    main()
