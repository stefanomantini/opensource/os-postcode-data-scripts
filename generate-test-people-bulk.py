import random, string, time
import connection

VOWELS = "aeiou"
CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
RESIDENCE = ["Scotland","England", "Wales", "Ireland"]


def generate_word(length):
    word = ""
    for i in range(length):
        if i % 2 == 0:
            word += random.choice(CONSONANTS)
        else:
            word += random.choice(VOWELS)
    return word


# DB & Collections
pcodes = connection.db.postcodes
people = connection.db.people
logger = connection.db.log


batch_size = 100000
number_of_iterations = 1
target = batch_size*number_of_iterations
print("Target Num Records to Create: " + str(target))

transactionID = generate_word(20)

count = 0
total_time = time.time()
for i in range(number_of_iterations):
    # get random in-use postcodes from dataset
    pipeline = [
        {"$match": {"In Use?": "Yes"}},
        {"$sample":{"size": batch_size}}
    ]
    opts = {
        "allowDiskUse": "true"
    }
    t0 = time.time()
    iterationID = generate_word(20)
    subquery = pcodes.aggregate(pipeline, allowDiskUse=True) # allow disk for large aggregation query
    qt0 = time.time() - t0
    person_batch = []
    for pcDoc in subquery:
        person = {}
        person["firstname"] = generate_word(random.randint(3,13))
        person["lastname"] = generate_word(random.randint(3,13))
        person["lastname"] = generate_word(random.randint(3,13))
        person["created"] = time.time()
        person["postcode"] = pcDoc["Postcode"]
        person_batch.append(person)
        count = count +1
    batchT_id = people.insert_many(person_batch).inserted_ids
    log = {}
    log["type"] = "generate_people_bulk"
    log["transactionID"] = transactionID
    log["iterationID"] = iterationID
    log["queryTime"] = qt0
    log["startTime"] = t0
    log["target"] = target
    log["iterationN"] = count
    log["endTime"] = time.time() 
    log["elapsedTime"] = time.time() - t0
    log["recordsProcessed"] = count
    log["throughput"] = count/(time.time() - t0)
    log_id = logger.insert_one(log)
    print(log)
    
print("Target Reached, Records Created: " +  str(count) + " in " + str(time.time()-total_time) + "s")
