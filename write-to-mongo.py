import csv
import app_constants
import connection

posts = connection.db.postcodes

def main():
    with open(app_constants.input_file) as f:
        reader = csv.DictReader(f)
        count = 0
        iterations = 1
        for row in reader:
            tempObj = {}
            tempObj["postcode"] = row["Postcode"]
            tempObj["county"] = row["County"]
            tempObj["district"] = row["District"]
            tempObj["country"] = row["Country"]
            tempObj["in use"] = row["In Use?"]
            tempObj["introduced"] = row["Introduced"]
            tempObj["terminated"] = row["Terminated"]
            tempObj["lastUpdated"] = row["Last updated"]
            post_id = posts.insert_one(tempObj).inserted_id
            if count > 1000:
                print("[INFO] Persisted record: " + str(post_id) + " Transactions: "+str(iterations*1000))
                count = 0
                iterations = iterations + 1
            count = count + 1

if __name__ == '__main__':
    main()
