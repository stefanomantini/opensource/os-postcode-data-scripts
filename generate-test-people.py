import random, string, time
import connection

VOWELS = "aeiou"
CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
RESIDENCE = ["Scotland", "England", "Wales", "Ireland"]

def generate_word(length):
    word = ""
    for i in range(length):
        if i % 2 == 0:
            word += random.choice(CONSONANTS)
        else:
            word += random.choice(VOWELS)
    return word


# DB & Collections
pcodes = connection.db.postcodes
people = connection.db.people
logger = connection.db.log

numIt = 50000000
t0 = time.time()
count = 0
transactionID = generate_word(20)
iterations = 1
print("Process running")
for i in range(numIt):
    # pipeline config for query
    pipeline = [
        {"$sample": {"size": 1}}
    ]
    subquery = pcodes.aggregate(pipeline)

    # create temp person
    person = {}
    person["firstname"] = generate_word(random.randint(3,13))
    person["lastname"] = generate_word(random.randint(3,13))
    person["lastname"] = generate_word(random.randint(3,13))
    person["created"] = time.time()
    person["postcode"] = [doc for doc in subquery][0]["Postcode"]

    # search for postcode and set residency#person["residency"] = RESIDENCE[random.randint(0, 3)]

    # send person
    person_id = people.insert_one(person).inserted_id
    if count > 1000:
        
        log = {}
        log["type"] = "generate_people"
        log["transactionID"] = transactionID
        log["startTime"] = t0
        log["endTime"] = time.time()
        log["elapsedTime"] = time.time() - t0
        log["recordsProcessed"] = count
        log["throughput"] = count/(time.time() - t0)
        log_id = logger.insert_one(log)
        print("Processed:"+str(iterations)+", Last log: "+str(log))
        count=0
    count = count + 1
    iterations = iterations + 1

    
print("Messages Consumed" + str(numIt) + " Since" + t0)
