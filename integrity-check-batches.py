import csv, re
import app_constants

n_postcodes = 0;
n_passes = 0;
fail_log = [];
chunkSize = 100000
file_output = "regex-check-failures.csv"

postcodeRegex = []

def main():

    with open(app_constants.input_file) as file:
        reader = csv.DictReader(file)
        for row in reader:
            n_postcodes = n_postcodes+1
            reg = re.compile('[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z][A-Z]')
            if(n_postcodes % chunkSize == 0):
                print(str(chunkSize)+" chunk evaluated, Tally of failures: " + str(len(fail_log)))

            if (reg.match(row["Postcode"])):
                pass
            else:
                failure_item = {}
                failure_item["postcode"] = row["Postcode"]
                failure_item["inUse"] = row["In Use?"]
                fail_log.append(failure_item)

        print("Postcodes Evaluated: "+str(n_postcodes))
        print("Failures Found: "+str(len(fail_log)))

        with open(file_output, 'w') as f:
            for item in fail_log:
                f.write("%s\n" % item["postcode"] + ", " + item["inUse"])
        print("Output written to file" + file_output)

if __name__ == '__main__':
    main()
